DELIMITER //
create trigger InsertTrueEmployeeIndentityNumber
before insert
on employee for each row
begin
	if (new.identity_number rlike '00$')
    then signal sqlstate'45000' set message_text = 'cant be 00 at the end of identity_number';
	end if;
end //
DELIMITER ;



DELIMITER //
create trigger UpdateTrueEmployeeIndentityNumber
before update
on employee for each row
begin
	if (new.identity_number rlike '00$')
    then signal sqlstate'45000' set message_text = 'cant be 00 at the end of identity_number';
	end if;
end //
DELIMITER ;


DELIMITER //
create trigger InsertTrueMedicineMinistryCode
before insert
on medicine for each row
begin
	if not(length(new.ministry_code)=7) 
    then signal sqlstate'45000' set message_text='ministry_code length should be 7';
	elseif (new.ministry_code rlike '^[_P][_M][P_][M_]')
    then signal sqlstate'45000' set message_text='first 2 letters cant be M or P';
    elseif not(new.ministry_code rlike '^__[0-9]%$')
	then signal sqlstate'45000' set message_text='sign 3-7 should be numbers';
    end if;
    
    set new.ministry_code = concat(substring(new.ministry_code, 1, 2), '-',
    substring(new.ministry_code, 3, 3), '-',
    substring(new.ministry_code, 6, 2));
    
    end //
DELIMITER ;

