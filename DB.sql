CREATE DATABASE StoredPr_DB
CHARACTER SET utf8 
COLLATE utf8_general_ci;

USE StoredPr_DB; 

CREATE TABLE employee(-- співробітник
    id                 INT               AUTO_INCREMENT,
    surname            VARCHAR(30)       NOT NULL,
    name               CHAR(30)          NOT NULL,
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
    experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15)       NOT NULL,
    pharmacy_id        INT,
    PRIMARY KEY (id)
)ENGINE=INNODB;

CREATE TABLE medicine( -- перелік лікарств
    id               INT            AUTO_INCREMENT,
    name             VARCHAR(30)    NOT NULL,
    ministry_code    CHAR(10),
    recipe           BIT(1),
    narcotic         BIT(1),
    psychotropic     BIT(1),
    PRIMARY KEY (id)
)ENGINE=INNODB;

CREATE TABLE medicine_zone( -- стикувальна ліки-зона впливу
    medicine_id    INT    NOT NULL,
    zone_id        INT    NOT NULL,
    PRIMARY KEY (medicine_id, zone_id)
)ENGINE=INNODB;

CREATE TABLE pharmacy( -- аптечна установа
    id                 INT            AUTO_INCREMENT,
    name               VARCHAR(15)    NOT NULL,
    building_number    VARCHAR(10),
    www                VARCHAR(40),
    work_time          TIME,
    saturday           BIT(1),
    sunday             BIT(1),
    street             VARCHAR(25),
    PRIMARY KEY (id)
)ENGINE=INNODB;

CREATE TABLE pharmacy_medicine( -- стикувальна аптека-ліки
    pharmacy_id    INT    NOT NULL,
    medicine_id    INT    NOT NULL,
    PRIMARY KEY (pharmacy_id, medicine_id)
)ENGINE=INNODB;

CREATE TABLE post( -- посада
    post    VARCHAR(15)    NOT NULL,
    PRIMARY KEY (post)
)ENGINE=INNODB;

CREATE TABLE street( -- вулиця
    street    VARCHAR(25)    NOT NULL,
    PRIMARY KEY (street)
)ENGINE=INNODB;

CREATE TABLE zone( -- зона впливу
    id      INT            AUTO_INCREMENT,
    name    VARCHAR(25)    NOT NULL,
    PRIMARY KEY (id)
)ENGINE=INNODB;

insert into employee(surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)
values('Omelychuk', 'Victor', 'Ivanovich', '1202304959', 'KL-234-45', 5, '1987-04-12', 'pharmacy', 1),
('Pavliv', 'Oleksander', 'Petrovich', '7854385769', 'OH-123-98', 2, '1997-03-23', 'doctor', 2);

insert into medicine(name, ministry_code, recipe, narcotic, psychotropic)
values('Paracetamol', '1322142', 1, 0, 0),
('Analgin', '3454675', 0, 1, 0);

insert into pharmacy(name, building_number, www, work_time, saturday, sunday, street)
values('pharmacy1', 12, 'pharmacy1@gmail.com', '08:00:00', 1, 0, 'Zelena'),
('pharmacy2', 21, 'pharmacy2@gmail.com', '06:00:00', 1, 1, 'Lisova');

insert into post(post)
value('pharmacy'),('doctor');

insert into street(street)
value('Zelena'),('Lisova');

insert into zone(name)
value('heart'),('head');

insert into medicine_zone(medicine_id, zone_id)
values(1,1),(2,2);

insert into pharmacy_medicine(pharmacy_id, medicine_id)
values(1,1),(2,2);


