DELIMITER //
create procedure InsertIntoMedicineZone(
	in inner_medicine_id int,
    in inner_zone_id int)
    begin
    if(exists(select id from medicine where id = inner_medicine_id)) 
    then
		if(exists(select id from zone where id = inner_zone_id)) 
		then insert into medicine_zone(medicine_id, zone_id)
		values(inner_medicine_id, inner_zone_id);
		end if;
    end if;
    end//
DELIMITER ;

DELIMITER //
create procedure CreateRandomTableEmployee()
begin
	set @random_size = ceil(rand()*10);
    set @iterator = 1;
    create table if not exists EmployeeRandom(id INT AUTO_INCREMENT primary key);
    case 
    when @random_size<2 
		then alter table EmployeeRandom add surname VARCHAR(30) NOT NULL;
        set_surname:loop
        if (exists(select surname from employee where id = @iterator)) then
        update EmployeeRandom set surname = (select surname from employee where id = @iterator);
        set @iterator = @iterat+1;
        else leave set_surname;
        end if;
        end loop;
        set @iterator = 1;
	when @random_size<3
		then alter table EmployeeRandom add name VARCHAR(30) NOT NULL;
        loop
        update EmployeeRandom set name = (select name from employee where id = @iterator);
        set @iterator = @iterat+1;
        end loop;
        set @iterator = 1;
	when @random_size<4
		then alter table EmployeeRandom add midle_name VARCHAR(30);
        loop
        update EmployeeRandom set midle_name = (select midle_name from employee where id = @iterator);
        set @iterator = @iterat+1;
        end loop;
        set @iterator = 1;
	when @random_size<5
		then alter table EmployeeRandom add identity_number CHAR(10);
        loop
        update EmployeeRandom set identity_number = (select identity_number from employee where id = @iterator);
        set @iterator = @iterat+1;
        end loop;
        set @iterator = 1;
	when @random_size<6
		then alter table EmployeeRandom add passport CHAR(10);
        loop
        update EmployeeRandom set passport = (select passport from employee where id = @iterator);
        set @iterator = @iterat+1;
        end loop;
        set @iterator = 1;
	when @random_size<7
		then alter table EmployeeRandom add experience DECIMAL(10, 1);
        loop
        update EmployeeRandom set experience = (select experience from employee where id = @iterator);
        set @iterator = @iterat+1;
        end loop;
        set @iterator = 1;
	when @random_size<8
		then alter table EmployeeRandom add birthday DATE;
        loop
        update EmployeeRandom set birthday = (select birthday from employee where id = @iterator);
        set @iterator = @iterat+1;
        end loop;
        set @iterator = 1;
	when @random_size<9
		then alter table EmployeeRandom add post VARCHAR(15) NOT NULL;
        loop
        update EmployeeRandom set post = (select post from employee where id = @iterator);
        set @iterator = @iterat+1;
        end loop;
        set @iterator = 1;
	when @random_size<10
		then alter table EmployeeRandom add pharmacy_id INT;
        loop
        update EmployeeRandom set pharmacy_id = (select pharmacy_id from employee where id = @iterator);
        set @iterator = @iterat+1;
        end loop;
        set @iterator = 1;
	else select * from employee;
	end case;
end//
DELIMITER ;

use storedpr_db;
drop procedure CreateRandomTableEmployee;
call InsertIntoMedicineZone(2,1);

call CreateRandomTableEmployee();