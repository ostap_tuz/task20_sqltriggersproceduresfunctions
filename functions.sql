DELIMITER //
create function SearchMinExperience()
returns DECIMAL(10, 1)
deterministic
begin
	 declare min_experience DECIMAL(10, 1);
     select min(experience) into  min_experience from employee;
     return min_experience;
end //
DELIMITER ;

select concat('Найменший стаж = ',SearchMinExperience()) as Min_experience;

DELIMITER //
create function SearchEmployeesInFarmacy(inner_id int)
returns varchar(100)
deterministic
begin
	 declare result varchar(100);
     declare em_name varchar(30);
     declare farm varchar(30);
     select name into em_name from employee where id = inner_id;
     select concat(name, ' ',building_number) into farm from pharmacy where id = inner_id;
     
     set result = concat(em_name, ' ', farm);
     return result;
end //
DELIMITER ;

select SearchEmployeesInFarmacy(1);
