USE StoredPr_DB; 

DELIMITER //
create trigger BeforeInsertEmployee
before insert 
on employee for each row
begin
	declare employee_id int;
    select max(id)+1 into employee_id from employee;
    if employee_id is null then set employee_id = 1;
    end if;
    if (not exists(select post from post where post=new.post)) 
    then signal sqlstate'45000' set message_text='there is no this element in table POST';
    end if;
    if (not exists(select id from pharmacy where id=new.pharmacy_id)) 
    then signal sqlstate'45000' set message_text='there is no this element in table PHARMACY';
    end if;
    
    set new.id = employee_id;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeInsertPharmacy
before insert 
on pharmacy for each row
begin
	declare inner_pharmacy_id int;
    select max(id)+1 into inner_pharmacy_id from pharmacy;
    if inner_pharmacy_id is null then set inner_pharmacy_id = 1;
    end if;
    if (not exists(select street from street where street=new.street)) 
    then signal sqlstate'45000' set message_text='there is no this element in table STREET';
    end if;

    set new.id = inner_pharmacy_id;
end //
DELIMITER ;



DELIMITER //
create trigger BeforeInsertMedicineZone
before insert
on  medicine_zone for each row
begin
	if(not exists(select id from medicine where id=medicine_id))
    then signal sqlstate'45000' set message_text='there is no this element in table MEDICINE';
    end if;
    if(not exists(select id from zone where id=zone_id))
    then signal sqlstate'45000' set message_text='there is no this element in table ZONE';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeInsertPharmacyMedicine
before insert
on  pharmacy_medicine for each row
begin
    if(not exists(select id from pharmacy where id=pharmacy_id))
    then signal sqlstate'45000' set message_text='there is no this element in table PHARMACY';
    end if;
    if(not exists(select id from medicine where id=medicine_id))
    then signal sqlstate'45000' set message_text='there is no this element in table MEDICINE';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeUpdateEmployee
before update
on employee for each row
begin
    if (not exists(select post from post where post=new.post)) 
    then signal sqlstate'45000' set message_text='there is no this element in table POST';
    end if;
    if (not exists(select id from pharmacy where id=new.pharmacy_id)) 
    then signal sqlstate'45000' set message_text='there is no this element in table PHARMACY';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeUpdatePharmacy
before update
on pharmacy for each row
begin
    if (not exists(select street from street where street=new.street)) 
    then signal sqlstate'45000' set message_text='there is no this element in table STREET';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeUpdateMedicineZone
before update
on  medicine_zone for each row
begin
	if(not exists(select id from medicine where id=medicine_id))
    then signal sqlstate'45000' set message_text='there is no this element in table MEDICINE';
    end if;
    if(not exists(select id from zone where id=zone_id))
    then signal sqlstate'45000' set message_text='there is no this element in table ZONE';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeUpdatePharmacyMedicine
before update
on  pharmacy_medicine for each row
begin
    if(not exists(select id from pharmacy where id=pharmacy_id))
    then signal sqlstate'45000' set message_text='there is no this element in table PHARMACY';
    end if;
    if(not exists(select id from medicine where id=medicine_id))
    then signal sqlstate'45000' set message_text='there is no this element in table MEDICINE';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeDeletePost
before delete
on post for each row
begin
	if(exists(select post from employee where post = old.post)) 
    then signal sqlstate'45000' set message_text = 'this element is used in table EMPLOYEE';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeDeleteStreet
before delete
on street for each row
begin
	if(exists(select street from street where street = old.street)) 
    then signal sqlstate'45000' set message_text = 'this element is used in table PHARMACY';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeDeletePharmacy
before delete
on pharmacy for each row
begin
	if(exists(select pharmacy_id from employee where pharmacy_id = old.id)) 
    then signal sqlstate'45000' set message_text = 'this element is used in table EMPLOYEE';
    end if;
    if(exists(select pharmacy_id from pharmacy_medicine where pharmacy_id = old.id)) 
    then signal sqlstate'45000' set message_text = 'this element is used in table PHARMACY_MEDICINE';
    end if;
end //
DELIMITER ;

DELIMITER //
create trigger BeforeDeleteMedicine
before delete
on medicine for each row
begin
    if(exists(select medicine_id from pharmacy_medicine where medicine_id = old.id)) 
    then signal sqlstate'45000' set message_text = 'this element is used in table PHARMACY_MEDICINE';
    end if;
    if(exists(select medicine_id from medicine_zone where medicine_id = old.id)) 
    then signal sqlstate'45000' set message_text = 'this element is used in table MEDICINE_ZONE';
    end if;
end //
DELIMITER ;

DELIMITER //
create trigger BeforeDeleteZone
before delete
on zone for each row
begin
    if(exists(select zone_id from medicine_zone where zone_id = old.id)) 
    then signal sqlstate'45000' set message_text = 'this element is used in table ZONE';
    end if;
end //
DELIMITER ;
